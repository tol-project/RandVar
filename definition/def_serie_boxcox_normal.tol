
//////////////////////////////////////////////////////////////////////////////
//BBL #RandVar::@Serie.BoxCoxNormal
Class @Serie.BoxCoxNormal : @Serie.Random, @Grammar.Distribution
//////////////////////////////////////////////////////////////////////////////
{
  // [Serie]
  //H: Set _.dating.;
  //H: Date _.begin;
  //H: Date _.end;
  // [Distribution]
  //H: Set _.parameters
  // [1] Real first (boxcox)
  // [2] Real second (boxcox)
  // [3] Serie mean
  // [4] Matrix covariance
  //  * 1x1: covarianza diagonal constante
  //  * nx1: covarianza diagonal (sin autocorrelaciones)
  //  * nxn: covarianza con autocorrelaciones
  // [5] Serie shift
  //  * Real: desplazamiento constante (complementario al atributo second)
  //  * Serie: desplazamiento variable

  ////////////////////////////////////////////////////////////////////////////
  // [Serie]

  //H: Anything GetGrammar(Real void)
  //H: TimeSet GetDating(Real void)
  //H: Date GetBegin(Real void)
  //H: Date GetEnd(Real void)

  ////////////////////////////////////////////////////////////////////////////
  // [Random]

  ////////////////////////////////////////////////////////////////////////////
  Serie _.GetNormalVariance(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Sqrt(_AsSerie(_.parameters::covariance, _.parameters::mean))
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix _.GetNormalCovariance(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Case(Rows(_.parameters::covariance)==1, {
      Real iLength = DateDif(GetDating(?), GetBegin(?), GetEnd(?)) + 1;
      Diag(iLength, MatDat(_.parameters::covariance, 1, 1))
    }, Columns(_.parameters::covariance)==1, {
      ColDiag(_.parameters::covariance)
    }, True, _.parameters::covariance)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything _GetTotalShift(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.parameters::shift + _.parameters::second
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetMean(Real void) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    BoxCox::Normal.Mean(_.parameters::first, _GetTotalShift(?), 
      _.parameters::mean, _.GetNormalVariance(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetVariance(Real void) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    BoxCox::Normal.Variance(_.parameters::first, _GetTotalShift(?), 
      _.parameters::mean, _.GetNormalVariance(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetSigma(Real void) //H: // StandardDeviation
  ////////////////////////////////////////////////////////////////////////////
  {
    Serie Sqrt(GetVariance(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix GetCovariance(Real void) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    BoxCox::Normal.Covariance(_.parameters::first, _GetTotalShift(?), 
      _.parameters::mean, _.GetNormalCovariance(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetMode(Real void) //H:
  ////////////////////////////////////////////////////////////////////////////
  { 
    BoxCox::Normal.Mode(_.parameters::first, _GetTotalShift(?), 
      _.parameters::mean, _.GetNormalVariance(?))
  };

  //H: Anything GetMedian(Real void)

  ////////////////////////////////////////////////////////////////////////////
  Anything GetQuantile(Real probability) //H:
  ////////////////////////////////////////////////////////////////////////////
  { 
    Serie nSigma = _.GetNormalVariance(?);
    Serie quantileN = _.parameters::mean + nSigma*DistNormalInv(probability);
    BoxCox::Inverse(_.parameters::first, _GetTotalShift(?), quantileN)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetConfidenceBands(Real alpha) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    Real alphaU = If(IsUnknown(alpha), 2*(1-DistNormal(2)), alpha);
    Set confidenceBands = [[
      Serie GetQuantile(alphaU/2),
      Serie GetQuantile(1-alphaU/2)
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetExperiment(Real void) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    Serie noise = If(Columns(_.parameters::covariance)>1, {
      Real length = DateDif(GetDating(?), GetBegin(?), GetEnd(?)) + 1;
      Matrix whNoise = Gaussian(length, 1, 0, 1);
      Matrix acovChol = Choleski(_.parameters::covariance);
      Serie acovNoise = MatSerSet(Tra(acovChol * whNoise), GetDating(?), 
        GetBegin(?))[1];
      Serie acovNoise + _.parameters::mean
    }, {
      Serie nSigma = _.GetNormalVariance(?); 
      Serie whNoise = SubSer(Gaussian(0, 1, GetDating(?)), GetBegin(?), 
        GetEnd(?));
      Serie whNoise * nSigma + _.parameters::mean
    });
    BoxCox::Inverse(_.parameters::first, _GetTotalShift(?), noise)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Otros

  ////////////////////////////////////////////////////////////////////////////
  Set GetSpc(Text name) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    Set Serie.BoxCoxNormal. = [[
      Text Subclass = "Serie.BoxCoxNormal";
      Text Name = name;
      Real BoxCox.First = _.parameters::first;
      Real BoxCox.Second = _.parameters::second;
      Serie Mean = _.parameters::mean; //+ Dating, Begin, End
      Matrix Covariance = _.parameters::covariance;
      Anything Shift = _.parameters::shift
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @Serie.BoxCoxNormal Spc(Set spc.) //H:~
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix covariance = If(ObjectExist("Serie", "spc.::Sigma"), {
      Tra(SerMat(spc.::Sigma))**2 // versión anterior (2.102 y anteriores)
    }, spc.::Covariance);
    Anything shift = If(ObjectExist("Anything", "spc.::Shift"), {
      spc.::Shift
    }, Real 0); // versión anterior (2.103 y anteriores)
    @Serie.BoxCoxNormal sbcn = @Serie.BoxCoxNormal::Multivariate_Shifted(
      spc.::BoxCox.First, spc.::BoxCox.Second, spc.::Mean, covariance, shift);
    PutLocalName(spc.::Name, sbcn)
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $Default%es Construye una variable aleatoria con distribución \
  //BBL normal transformada (siguiendo una BoxCox) a partir de los \
  //BBL parámetros de la transformación y de la media y la sigma de la \
  //BBL distribución.
  Static @Serie.BoxCoxNormal Default(Real first, Real second, Serie mean, 
    Anything sigma)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie.BoxCoxNormal::New([[
      Real _.first = first;
      Real _.second = second;
      Serie _.mean = mean;
      Anything _.sigma = sigma
    ]])    
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $Multivariate%es Construye una variable aleatoria con distribución \
  //BBL normal multivariante transformada (siguiendo una BoxCox) a partir de \
  //BBL los parámetros de la transformación y de la media y la matriz de \
  //BBL covarianzas de la distribución.
  Static @Serie.BoxCoxNormal Multivariate(Real first, Real second, 
    Serie mean, Matrix covariance)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie.BoxCoxNormal::New([[
      Real _.first = first;
      Real _.second = second;
      Serie _.mean = mean;
      Matrix _.covariance = covariance
    ]])    
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $Multivariate_Shifted%es Construye una variable aleatoria con \
  //BBL distribución normal multivariante transformada (siguiendo una \
  //BBL BoxCox) y desplazada, a partir de los parámetros de la \
  //BBL transformación, de la media y la matriz de covarianzas de la \
  //BBL distribución y de una serie de desplazamiento.
  Static @Serie.BoxCoxNormal Multivariate_Shifted(Real first, Real second, 
    Serie mean, Matrix covariance, Anything shift)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie.BoxCoxNormal::New([[
      Real _.first = first;
      Real _.second = second;
      Serie _.mean = mean;
      Matrix _.covariance = covariance;
      Anything _.shift = shift
    ]])    
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $New%es (Uso interno) Construye una variable aleatoria serie con \
  //BBL distribución normal transformada Boxcox a partir de un nameblock \
  //BBL de argumentos.
  Static @Serie.BoxCoxNormal New(NameBlock args)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real nerror = Copy(NError);
    Real first = args::_.first;
    Real second = args::_.second;
    // El espacio temporal viene dado por la serie de medias.
    // Se comprueba si la serie tiene un fechado y límites válidos y si 
    // presenta valores finitos:
    Serie mean = _PrepareSerie(args::_.mean);
    // La matriz de covarianzas puede indicarse de varias formas:
    //  * Por un valor real
    //  * Por una serie temporal
    //  * Por una matriz
    // El atributo _.parameters::covariance será una matriz con dimensiones:
    //  * 1x1, si es diagonal con valores constantes
    //  * nx1, si es diagonal con distintos valores (es la diagonal)
    //  * nxn, si no es diagonal
    Matrix covariance = Case(NError!=nerror, {
      Rand(0,0,0,0)
    }, ObjectExist("Real", "args::_.variance"), {
      Row(args::_.variance)
    }, ObjectExist("Serie", "args::_.variance"), {
      Serie variance = _PrepareSerie_Ref(args::_.variance, mean);
      Tra(SerMat(variance))
    }, ObjectExist("Matrix", "args::_.variance"), {
      _PrepareCovariance_Ref(args::_.variance, mean)
    }, ObjectExist("Matrix", "args::_.covariance"), {
      _PrepareCovariance_Ref(args::_.covariance, mean)
    }, ObjectExist("Real", "args::_.sigma"), {
      Row(args::_.sigma^2)
    }, ObjectExist("Serie", "args::_.sigma"), {
      Serie sigma = _PrepareSerie_Ref(args::_.sigma, mean);
      Tra(SerMat(sigma^2))
    });
    Anything shift = If(ObjectExist("Serie", "args::_.shift"), {
      Serie _PrepareSerie_Ref(args::_.shift, mean)
    }, Real 0);
    If(NError==nerror, {
      @Serie.BoxCoxNormal serieBoxCoxNormal = [[
       Set _.dating. = [[ Dating(mean) ]];
        Date _.begin = First(mean);
        Date _.end = Last(mean);
        Set _.parameters = [[
          Real PutLocalName("first", first);
          Real PutLocalName("second", second);
          Serie PutLocalName("mean", mean);
          Matrix PutLocalName("covariance", covariance);
          Anything PutLocalName("shift", shift)
        ]]
      ]]
    })
  }
};
//////////////////////////////////////////////////////////////////////////////
