
//////////////////////////////////////////////////////////////////////////////
//BBL #RandVar::@Matrix.Sample
Class @Matrix.Sample : @Matrix.Random, @Grammar.Sample
//////////////////////////////////////////////////////////////////////////////
{
  // [Matrix]
  //H: Real _.rows;
  //H: Real _.columns;
  // [Sample]
  //H: Text _sampler;
  //H: Matrix _sample;

  ////////////////////////////////////////////////////////////////////////////
  // [Matrix]

  //H: Anything GetGrammar(Real void)
  //H: Real GetRows(Real void)
  //H: Real GetColumns(Real void)

  ////////////////////////////////////////////////////////////////////////////
  Anything _ToGrammar(Matrix columnVector)
  ////////////////////////////////////////////////////////////////////////////
  { ColumnVector::ToMatrix(columnVector, _.rows) };

  ////////////////////////////////////////////////////////////////////////////
  Set _ToGrammars(Matrix columnVector)
  ////////////////////////////////////////////////////////////////////////////
  { ColumnVector::ToMatrices(columnVector, _.rows) };

  ////////////////////////////////////////////////////////////////////////////
  Real _GetSampleRow(Real row, Real column)
  ////////////////////////////////////////////////////////////////////////////
  { (column-1)*_.rows+row };

  ////////////////////////////////////////////////////////////////////////////
  @Real.Random GetElement(Real row, Real column) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    @Real.Sample::Sampler(_sampler, 
      SubRow(_sample, _GetSampleRow(row, column)))
  };

  ////////////////////////////////////////////////////////////////////////////
  // [Random]

  //H: Anything GetMean(Real void)
  //H: Anything GetVariance(Real void)
  //H: Anything GetSigma(Real void)
  //H: Anything GetMedian(Real void)
  //H: Anything GetQuantile(Real probability)
  //H: Set GetConfidenceBands(Real alpha)
  //H: Anything GetExperiment(Real void)

  ////////////////////////////////////////////////////////////////////////////
  // [Sample]

  //H: Text GetSampler(Real void)
  //H: Matrix GetSample(Real void)
  //H: Real GetSize(Real void)
  //H: Set GetExperiments(Real void)
  //H: Anything GetExperiment_Index(Real index)

  ////////////////////////////////////////////////////////////////////////////
  // Otros

  ////////////////////////////////////////////////////////////////////////////
  Set GetSpc(Text name) //H:
  ////////////////////////////////////////////////////////////////////////////
  {
    Set Matrix.Sample. = [[
      Text Subclass = "Matrix.Sample";
      Text Name = name;
      Real Rows = _.rows;
      Real Columns = _.columns;
      Text Sampler = _sampler;
      Matrix Sample = _sample
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @Matrix.Sample Spc(Set spc.) //H:~
  ////////////////////////////////////////////////////////////////////////////
  {
    @Matrix.Sample ms = Case(ObjectExist("Matrix", "spc.::Sample"), {
      @Matrix.Sample::Sampler(spc.::Sample, spc.::Sampler)
    }, ObjectExist("Set", "spc.::Experiments"), { // Especificaci�n antigua
      @Matrix.Sample::Sampler(spc.::Experiments, spc.::Sampler)
    });
    PutName(spc.::Name, ms)
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $Default%es =#RandVar::@Grammar.Sample$Default
  Static @Matrix.Sample Default(Set sample)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Matrix.Sample::Sampler(sample, @Grammar.Sample::ObtainRandomSampler(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $Sampler%es =#RandVar::@Grammar.Sample$Sampler
  Static @Matrix.Sample Sampler(Set sample, Text sampler)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(Card(sample), {
      Real rows = Rows(sample[1]);
      Real columns = Columns(sample[1]);
      @Matrix.Sample matrixSample = [[
        Real _.rows = rows;
        Real _.columns = columns;
        Text _sampler = sampler;
        Matrix _sample = ColumnVector::FromMatrices(sample)
      ]]
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $ByColumns%es Construye una variable aleatoria matriz de tipo \
  //BBL muestraa partir de los experimentos en forma de una matriz (cada \
  //BBL matriz en una fila), el n�mero de filas y columna de la matrix y el \
  //BBL identificador del muestreo.
  Static @Matrix.Sample ByColumns(Matrix columnVector, Real rows, 
    Real columns, Text sampler)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Matrix.Sample matrixSample = [[
      Real _.rows = rows;
      Real _.columns = columns;
      Text _sampler = sampler;
      Matrix _sample = columnVector
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $FromMatrixAndValues%es Construye una variable aleatoria matriz de \
  //BBL tipo muestra a partir de una matriz (determinista) \
  //BBL modificando determinados algunos elementos a trav�s de variables \
  //BBL aleatorias reales indicadas en un conjunto de pares posici�n-valor.
  Static @Matrix.Sample FromMatrixAndValues(Matrix matrix, 
    Set position.values) // pares (par fila-columna)-valor
  ////////////////////////////////////////////////////////////////////////////
  {
    Real columns = Columns(matrix);
    Real rows = Rows(matrix);
    Text sampler = (position.values[1][2])::GetSampler(?);
    Real sampleLength = (position.values[1][2])::GetSize(?);
    Matrix columnVector = Group("ConcatColumns", For(1, sampleLength, 
      Matrix (Real i) {
      ColumnVector::FromMatrix(matrix)
    }));
    Real cdRows = Rows(columnVector);
    Set EvalSet(position.values, Real (Set position.value) {
      Real row = MatDat(position.value[1], 1, 1);
      Real column = MatDat(position.value[1], 1, 2);
      Real cdRow = (column-1)*rows+row;
      If(cdRow>=1 & cdRow<=cdRows, {
        Matrix data = (position.value[2])::GetSample(?); // una fila
        Matrix columnVector := SubRow(columnVector, Range(1, cdRow-1, 1)) 
          << data << SubRow(columnVector, Range(cdRow+1, cdRows, 1));
      1}, 0)
    });
    @Matrix.Sample::ByColumns(columnVector, rows, columns, sampler)
  }
};
//////////////////////////////////////////////////////////////////////////////
