
proc AddMarkerX {chartID markerPos index} {
  set g [$chartID getframe].g.gr.gr
  set this [::bayesGraph::getInstance $g]
  upvar \#0 ${this}::options opt
  set color [lindex $opt(gr,0,colors) [expr $index-1]]
  foreach ms $markerPos {
    foreach p $ms {
      $g marker create line -coord [list $p -Inf $p Inf] -outline $color
    }
  }
}
